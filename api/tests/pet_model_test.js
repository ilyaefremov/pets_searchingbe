const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const { accountManager } = require("../lib/extensions")
const utils = require("../lib/utils")
const { GENDER } = utils.getConstants();
const mongoose = require('mongoose');
const crypto = require('crypto');

const expect = chai.expect;
const assertArrays = require('chai-arrays');

chai.use(assertArrays);
chai.use(chaiHttp);
chai.should();

describe('Pet model tests', () => {
    let USER_ACCESS_TOKEN;
    let USER_TWO_ACCESS_TOKEN;
    let PET_ID;
    let TEST_PET_ID;

    const clearDatabase = async () => {
        await mongoose.models.User.remove({});
        await mongoose.models.Pet.remove({});
        await mongoose.models.Breed.remove({});
    }

    before(async function() {
        await clearDatabase();

        const user = await mongoose.models.User.create({ firebase_user_id: crypto.randomBytes(16).toString('hex') })
        await user.refreshToken();
        USER_ACCESS_TOKEN = user.access_tokens[0];

        const userTwo = await mongoose.models.User.create({ firebase_user_id: crypto.randomBytes(16).toString('hex') })
        await userTwo.refreshToken();
        USER_TWO_ACCESS_TOKEN = userTwo.access_tokens[0];

        const breed = await mongoose.models.Breed.create({ breed: "samoyed" })

        const pet = await mongoose.models.Pet.create({ name: "Maya", breed_id: breed._id, color: "White", age: "1", description: "Good girl", gender:"female", longitude: "1", latitude: "1", user_id: user._id })    
        await mongoose.models.Pet.create({ name: "Rey", breed_id: breed._id, color: "Brown", age: "2", description: "Mr Dr Prof Kakashek", gender:"male", longitude: "1", latitude: "1", user_id: user._id })   
        PET_ID = pet._id

    });
  
    after(async function() {
        await clearDatabase();
    });



    /*
     * SHOW PETS
     */
    describe('GET /pets/', () => {

        it('should return error when unauthenticated', (done) => {
            chai.request(app).get(`/pets/`).end((err, res) => {
                expect(res.status, res.status).to.be.equal(401);
                done();
            });
        });

        it('should return error when wrong access_token passed', (done) => {
            chai.request(app).get(`/pets/`).set('authorization', "invalid access_token").end((err, res) => {
                expect(res.status, res.status).to.be.equal(401);
                done();
            });
        });

        it('should return success', (done) => {
            chai.request(app).get(`/pets/`).set('authorization', `${USER_ACCESS_TOKEN}`).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(200);
                expect(res.body.success, "res.body.success").to.be.true;
                expect(res.body.data, "res.body.data").to.be.an('object');
                expect(res.body.data.pets, "res.body.data.pets").to.be.an('array');
                expect(res.body.data.pets, "res.body.data.pets").to.have.lengthOf(2);
                done();
            });
        });

        it('should show only one pet with filter', (done) => {
            chai.request(app).get(`/pets?age=1`).set('authorization', `${USER_ACCESS_TOKEN}`).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(200);
                expect(res.body.success, "res.body.success").to.be.true;
                expect(res.body.data, "res.body.data").to.be.an('object');
                expect(res.body.data.pets, "res.body.data.pets").to.be.an('array');
                expect(res.body.data.pets, "res.body.data.pets").to.have.lengthOf(1);
                done();
            });
        });

    })



    /*
     * CREATE PET
     */
    describe('GET /pets/', () => {

        it('should return error when unauthenticated', (done) => {
            chai.request(app).post(`/pets/`).end((err, res) => {
                expect(res.status, res.status).to.be.equal(401);
                done();
            });
        });

        it('should return error when wrong access_token passed', (done) => {
            chai.request(app).post(`/pets/`).set('authorization', "invalid access_token").end((err, res) => {
                expect(res.status, res.status).to.be.equal(401);
                done();
            });
        });

        it('should return error when not existing breed is passed', (done) => {
            const data = {name: "Donut", breed: "Corgi", color: "Brown", age: "2", description: "Good boy", gender: "male", longitude: "1", latitude: "1"}
            chai.request(app).post(`/pets/`).set('authorization', `${USER_ACCESS_TOKEN}`).send(data).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(200);
                expect(res.body.success, "res.body.success").to.be.false;
                done();
            });
        });

        it('should return success', (done) => {

            const data = { name: "Donut", breed: "Samoyed", color: "Brown", age: "2", description: "Good boy", gender: "male", longitude: "1", latitude: "1" }
            chai.request(app).post(`/pets/`).set('authorization', `${USER_ACCESS_TOKEN}`).send(data).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(200);
                expect(res.body.success, "res.body.success").to.be.true;
                expect(res.body.data, "res.body.data").to.be.an('object');
                TEST_PET_ID = res.body.data.id
            });


            /*
             * Check that object has been added
             */
            chai.request(app).get(`/pets?name=Donut&breed=Samoyed&color=Brown`).set('authorization', `${USER_ACCESS_TOKEN}`).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(200);
                expect(res.body.success, "res.body.success").to.be.true;
                expect(res.body.data, "res.body.data").to.be.an('object');
                expect(res.body.data.pets, "res.body.data.pets").to.be.an('array');
                expect(res.body.data.pets, "res.body.data.pets").to.have.lengthOf(1);
                expect(res.body.data.pets[0], "res.body.data.pets[0]").to.have.property("id", TEST_PET_ID)
                done();
            });


        });

    })



    /*
     * DELETE PET
     */
    describe('DELETE /pets/:pet_id/delete', () => {

        it('should return error when unauthenticated', (done) => {
            chai.request(app).delete(`/pets/${PET_ID}/delete`).end((err, res) => {
                expect(res.status, res.status).to.be.equal(401);
                done();
            });
        });

        it('should return error when wrong access_token passed', (done) => {
            chai.request(app).delete(`/pets/${PET_ID}/delete`).set('authorization', "invalid access_token").end((err, res) => {
                expect(res.status, res.status).to.be.equal(401);
                done();
            });
        });

        it('should return error when pet does not belong to user', (done) => {
            chai.request(app).delete(`/pets/${PET_ID}/delete`).set('authorization', `${USER_TWO_ACCESS_TOKEN}`).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(403);
                done();
            });
        });

        it('should return error when wrong pet_id passed', (done) => {
            chai.request(app).delete(`/pets/${11111}/delete`).set('authorization', `${USER_ACCESS_TOKEN}`).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(200);
                expect(res.body.success, "res.body.success").to.be.false;
                done();
            });
        });

        it('should return success when pet deleted', (done) => {
            chai.request(app).delete(`/pets/${PET_ID}/delete`).set('authorization', `${USER_ACCESS_TOKEN}`).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(200);
                expect(res.body.success, "res.body.success").to.be.true;
            });

            /*
             * Check that object doesn't exist anymore
             */
            chai.request(app).get(`/pets?name=Maya&color=White`).set('authorization', `${USER_ACCESS_TOKEN}`).end((err, res) => {
                expect(res.status, "res.status").to.be.equal(200);
                expect(res.body.success, "res.body.success").to.be.true;
                expect(res.body.data, "res.body.data").to.be.an('object');
                expect(res.body.data.pets, "res.body.data.pets").to.be.an('array');
                expect(res.body.data.pets, "res.body.data.pets").to.have.lengthOf(0);
                done();
            });
        });

    })
  
});