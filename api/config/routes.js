const controllers = require('../controllers/controllers');

const routes = [
  { path: '/users', controller: controllers.users },  
  { path: '/pets', controller: controllers.pets },   
  { path: '/uploads', controller: controllers.uploads },  
  { path: '/chats', controller: controllers.chats },   
  { path: '/messages', controller: controllers.messages },  
  { path: '/docs', controller: controllers.docs },  
 
];

exports.activate = app => {
  routes.forEach(route => {
    app.use(route.path, route.controller);
  });
};
