const express = require('express');
const bodyParser = require('body-parser');
// GOOGLE_APPLICATION_CREDENTIALS=/var/www/kma_be/config/service-account-file.json pm2 start app.js

const cron = require("node-cron");

const app = express();
const http = require('http');
const mongoose = require('mongoose');
const path = require("path");

const io = require("socket.io")(http);

require("../shared/model-importer")(mongoose, path.join(__dirname, './methods'));


const admin = require('firebase-admin');
const cors = require('cors');

const middleware = require("./lib/middleware");
require('express-async-errors');

const routes = require('./config/routes');
const masterConfig = require('./config/config');


const environment = process.env.ENV || 'dev';
const serverConfig = masterConfig.server[environment];
const dbConfig = masterConfig.mongo[environment];

const serviceAccount = require("../shared/config/serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://style-a34ed.firebaseio.com"
});

app.use(cors({ origin: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));

app.use((req, res, next) => {
  if (req.query._method) req.method = req.query._method;

  res.sendResponse = function sendResponse(data) {
    return middleware.utils.sendResponse(res, data);
  };

  res.forbidden = function () {
    return res.sendStatus(403);
  }

  console.log(`${req.method} ${req.url}`);
  next();
});

routes.activate(app);

app.use((req, res) => {
  return middleware.utils.sendError(res, 404, 'Route not found');
});

app.use((error, req, res, next) => {
  console.log(error)
  return middleware.utils.sendError(res, error.message);
});

const { port } = serverConfig;
app.set('port', port);

mongoose
  .connect(dbConfig.uri, dbConfig.options)
  .then(async () => {
    http.createServer(app).listen(port);
    

    //setupCronJobs();
  })
  .catch(error => {
    console.log(error);
  });



module.exports = app;

// require("../shared/model-importer")(mongoose, path.join("/var/www/pet.gotechmakers.com/pets_searchingbe/api", './methods'));

//const mongoose = require("mongoose");

//require("../shared/model-importer")(mongoose, path.join("/Users/iliaefremov/node_projects/pet_search/api", './methods'));

//const dbConfig = require('./config/config').mongo[process.env.ENV || "dev"];
//mongoose.connect(dbConfig.uri, dbConfig.options)
//mongoose.models.Pet.find({}).then(n => a = n)
//mongoose.models.Chat.create({user_ids: [ {user_id: "5f3e90dba1a3fa3fd54b7acb"} , {user_id: "5f589533c9f9c10a1a331657" }}).then(n => notifications = n)
//mongoose.models.Message.create({user_id: "5f3e90dba1a3fa3fd54b7acb", chat_id: "5f7f8b243ed82a20f1426e9c", message: "Sup"}).then(n => notifications = n)

