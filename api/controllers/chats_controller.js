const router = require('express').Router({ mergeParams: true });
const { User, Chat, Message } = require('mongoose').models;

const { authenticators, fetchers, utils } = require("../lib/middleware");

const pusher = require("../app").pusher

const isNull = (value) => typeof value === "object" && !value

router.get("/", authenticators.user, async (req, res) => {
	const { user } = req;

	const chats = await Chat.find({user_ids: {$elemMatch: {user_id: user._id}}});

	let result = await Chat.exportObjects(chats, user)

	result.sort((a,b) => (a.last_message.created_at < b.last_message.created_at) ? 1 : ((b.last_message.created_at < a.last_message.created_at) ? -1 : 0));

	return res.sendResponse({chats: result});
});

router.post("/", authenticators.user, async (req, res) => {
	const { user, body } = req;
	const { recipient_id: recipientId } = body;

	const user_ids = [ { user_id: user._id }, { user_id: recipientId } ]

	const chat = await Chat.createObject(user_ids)

	const result = await chat.exportObject(user)

	return res.sendResponse({ chat: result });
});

router.get("/exist", authenticators.user, async (req, res) => {
	const { user, body } = req;
	const { recipient_id: recipientId } = body;

	const chat = await Chat.find({ $and: [ { user_ids: { $elemMatch : { user_id: user._id }}}, { user_ids: { $elemMatch : { user_id: recipientId }}}] });

	const result = {
		exist: !(chat.length == 0),
		chat: !(chat.length == 0) ? await Chat.exportObjects(chat, user) : null
	}

	return res.sendResponse({ result: result });
});

router.get("/test", async (req, res) => {
	const chats = await Chat.find({});
	return res.sendResponse(chats);
});


module.exports = router;
