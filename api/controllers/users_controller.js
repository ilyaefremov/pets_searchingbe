const router = require('express').Router({ mergeParams: true });
const { User } = require('mongoose').models;
const { authenticators, fetchers, utils } = require("../lib/middleware");
const fs = require('fs');
const path = require('path') 

router.post("/", async (req, res) => {
  	const { body } = req;

	const user = await User.createObject(body);
	const token = await user.refreshToken();

	const profile = await user.exportObject();

	return res.sendResponse({ token, profile });
});

router.get("/test", async (req, res) => {
	const users = await User.find({});
	return res.sendResponse(users);
});

router.get("/privacy", async (req, res) => {
	const content = fs.readFileSync(path.join(__dirname, '../docs/privacy.html'), 'utf8');
	return res.sendResponse({ content });
});

router.put("/me/fcm_token", authenticators.user, async (req, res) => {
  	const { body, user } = req;
  	const { fcm_token : fcmToken } = body;
  	
  	if (!fcmToken) throw new Error("fcm_token is empty");
	await user.refreshFcmToken(fcmToken);
	
	return res.sendResponse();
});

router.put("/logout", authenticators.user, async (req, res) => {
 	const { user, body } = req;
 	const { access_token : accessToken, fcm_token : fcmToken } = body;

 	await user.logoutUser(accessToken, fcmToken);

	return res.sendResponse();
});

router.post('/pusher/auth/presence', authenticators.user, async (req, res) => {
	const { body, user } = req;
	const { socket_id: socketId, channel_name: channelName } = body


    const presenceData = {
		user_id: user._id,
		user_info: {
            name: user.name
        }
	}

	return res.sendResponse(utils.pusher.authenticate(socketId, channelName, presenceData)); 
});

router.post("/bookmarks", authenticators.user, async (req, res) => {
	const { body, user } = req;
	const { pet_id: petId } = body;

	for (let i = 0; i < user.saved_pets.length; i++){
		if (user.saved_pets[i].pet_id.equals(petId)){
			user.saved_pets.splice(i, 1)
			await user.save()
			return res.sendResponse();
		}
	}

	user.saved_pets.push({ pet_id: petId });

	await user.save();

	return res.sendResponse();
})

router.get("/bookmarks", authenticators.user, async (req, res) => {
	const { user } = req;

	const pets = await user.exportBookmarks();

	return res.sendResponse({ pets: pets });
})

router
	.route("/me")
		.all(authenticators.user, (req, res, next) => {
			return next();
		})
		.get( async (req, res) => {
		 	const { user } = req;
		 	const result = await user.exportObject()

			return res.sendResponse(result);
		})
		.put( async (req, res) => {
			const { user, body } = req;
			await user.updateObject(body);

			return res.sendResponse();
		});

router.get("/money_heist", async (req, res) => {


	res.render('./money_heist');
})

module.exports = router;
