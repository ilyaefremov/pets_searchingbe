const router = require('express').Router({ mergeParams: true });
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../config/swagger.json');

/*
* GET /docs
* @description View swagger docs
*/
router.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


module.exports = router;
