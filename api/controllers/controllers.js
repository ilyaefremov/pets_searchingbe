const users = require("./users_controller");
const pets = require("./pets_controller");
const uploads = require("./uploads_controller");
const chats = require("./chats_controller");
const messages = require("./messages_controller");
const docs = require("./docs_controller");

module.exports = {
	users,
	pets,	
	uploads,
	messages,
	chats,
	docs
};