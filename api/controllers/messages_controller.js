const router = require('express').Router({ mergeParams: true });
const { User, Chat, Message } = require('mongoose').models;

const { authenticators, fetchers, utils } = require("../lib/middleware");

router.post("/", authenticators.user, async (req, res) => {
  	const { body, user } = req;
  	const { chat_id } = body

	const messages = await Message.getMessages(chat_id)
	const result = await Message.exportObjects(messages)

	return res.sendResponse({ messages: result });
});

router.post('/pusher/auth/presence', authenticators.user, async (req, res) => {
	const { body, user } = req;
	const { socket_id: socketId, chanell_name: chanellName } = body

    let presenceData = {
		user_id: user._id,
		user_info: { name: user.name } 
	};

	let auth = utils.pusher.authenticate(socketId, chanellName, presenceData);

	return res.sendResponse({ auth: auth });  
});

router.post('/pusher/auth/private', authenticators.user, async (req, res) => {
	const { body, user } = req;
	const { socket_id: socketId, channel_name: channelName } = body
	res.send(utils.pusher.authenticate(socketId, channelName));
});

router.post('/send-message', authenticators.user, async (req, res) => { 
	const { body, user } = req;
	const { text, chat_id: chatId, channel_name: channelName } = body;

	const message = await Message.createObject(text, user, chatId)
	const result = await message.exportObject();

	await Message.sendPush(result, chatId);

	if (channelName != null){
    	let payload = { message: result, sender_id: user._id }
    	utils.pusher.trigger(channelName, 'new-message', payload);
	}

    return res.sendResponse({ message: result });
});

router.put('/set_messages_red', authenticators.user, async (req, res) => { 
	const { body, user } = req;
	const { chat_id: chatId, channel_name: channelName } = body;

	const chat = await Chat.findById(chatId)
    const chatExported = await chat.exportObject(user)

	const result = await Message.setRed(chatExported)

	const payload = { whos_red: chatExported.user.id }
    utils.pusher.trigger(channelName, 'messages-red', payload);

    return res.sendResponse();
});

module.exports = router;
