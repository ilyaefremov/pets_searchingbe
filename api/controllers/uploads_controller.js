const router = require('express').Router({ mergeParams: true });
const { FilePointer } = require('mongoose').models;
const path = require('path') 

const USER_IMAGES_FOLDER = "users_images";

const multer  = require('multer')
const upload = multer({ dest: path.join(__dirname, `../public/${USER_IMAGES_FOLDER}`) })

const { authenticators } = require("../lib/middleware");

router.post("/", authenticators.user, upload.single('file'), async (req, res) => {
  	const { file } = req;

  	if (!file) throw new Error("file is empty")
	
	const object = await FilePointer.createObject(`/${USER_IMAGES_FOLDER}/${file.filename}`);
	const result = await object.exportObject();

	return res.sendResponse(result);
});

router.post("/multiple",  upload.array('files', 5), async (req, res) => {
  	const { files } = req;
  	let resultFiles = []

  	if (!files) throw new Error("files are empty")
	
	for (let i = 0; i < files.length; i++){
		let object = await FilePointer.createObject(`/${USER_IMAGES_FOLDER}/${files[i].filename}`);
		let result = await object.exportObject();
		resultFiles[i] = result;
	}

	return res.sendResponse({result: resultFiles});
});

module.exports = router;