const router = require('express').Router({ mergeParams: true });
const { Pet, User } = require('mongoose').models;
const mongoose = require('mongoose');

const { authenticators, fetchers } = require("../lib/middleware");

const tfnode = require('@tensorflow/tfjs-node');
const mobilenet = require('@tensorflow-models/mobilenet');

const fs = require('fs');

const path = require('path') 

const USER_IMAGES_FOLDER = "users_images";
const multer  = require('multer')
const upload = multer({ dest: path.join(__dirname, `../public/${USER_IMAGES_FOLDER}`) })


router.get("/", authenticators.user, async (req, res) => {
	const { user, body } = req;

	const pets = await Pet.findObjects(body, body.page)
	const result = await Pet.exportObjects(pets[1])

	return res.sendResponse({ length: pets[0], pets: result });
});

router.post("/map", authenticators.user, async (req, res) => {
	const { user, body } = req;

	const pets = await Pet.findMapObjects(body)
	const result = await Pet.exportObjects(pets)

	return res.sendResponse({ pets: result });
});

router.post("/", authenticators.user, async (req, res) => {
	const { user, body } = req;

	const pet = await Pet.createObject(body, user);
	const result = await pet.exportObject();

	return res.sendResponse(result);
});


router.get("/archived", authenticators.user, async (req, res) => {
	const { user, body } = req;
	const pets = await Pet.getArchived(user._id)

	const result = await Pet.exportObjects(pets);

	return res.sendResponse({ pets: result });
});


router.put("/:pet_id/change_status", authenticators.user, fetchers.pet, async (req, res) => {
	const { user, pet, body } = req;

	if (!user._id.equals(pet.user_id)) return res.forbidden()

	const result = await pet.changeStatus();

	return res.sendResponse();
});


router.delete("/:pet_id/delete", authenticators.user, fetchers.pet, async (req, res) => {
	const { user, pet, body } = req;

	if (!user._id.equals(pet.user_id)) return res.forbidden()

	return res.sendResponse(await pet.deleteObject());
});


router.put("/:pet_id/update", authenticators.user, fetchers.pet, async (req, res) => {
	const { user, pet, body } = req;

	if (!user._id.equals(pet.user_id)) return res.forbidden()

	await pet.updateObject(body);

	return res.sendResponse();
});


router.put("/:pet_id/update_images", authenticators.user, fetchers.pet, async (req, res) => {
	const { user, pet, body } = req;
	const { images_id: imagesId } = body;

	if (!user._id.equals(pet.user_id)) return res.forbidden()
	if (pet.secondary_images.length > 5) return res.sendError("Amount of images can't be greater than 5")

	const result = await pet.updateImages(imagesId);

	return res.sendResponse();
});


router.delete("/:pet_id/delete_image", authenticators.user, fetchers.pet, async (req, res) => {
	const { user, pet, body } = req;
	const { image_url: imageUrl } = body;

	if (!user._id.equals(pet.user_id)) return res.forbidden()

	const result = await pet.deleteImage(imageUrl)

	return res.sendResponse(result);
});


router.get("/my", authenticators.user, async (req, res) => {
	const { user, body } = req;
	const pets = await Pet.find({ user_id: user._id, is_archived: false }).sort('-created_at');

	const result = await Pet.exportObjects(pets);

	return res.sendResponse({ pets: result });
});


router.post("/fill_breeds", async (req, res) => {
	const breeds = require('../breeds.json').breeds
	const breedsEn = require('../breed_name.json').breeds
	const breedsRu = require('../breed_name_ru.json').breeds

	await mongoose.models.Pet.remove({})
	await mongoose.models.Breed.remove({})

	for (let i = 0; i < breeds.length; i++){
		let her = await mongoose.models.Breed.create({ breed: breeds[i]})
		her.localized_breeds.push({localized_breed: breedsEn[i]})
		her.localized_breeds.push({localized_breed: breedsRu[i]})
		her.save()
	}
	
	return res.sendResponse(await mongoose.models.Breed.find({}));
});


const readImage = path => {
	const imageBuffer = fs.readFileSync(path);
	
	const tfimage = tfnode.node.decodeImage(imageBuffer);
	return tfimage;
}

router.post("/breed_finder", upload.single('file'), async (req, res) => {
	const { file } = req;

  	if (!file) throw new Error("file is empty")

  	const image = readImage(file.path);

	const mobilenetModel = await mobilenet.load();
	const predictions = await mobilenetModel.classify(image);

	if (!predictions) return res.sendResponse({ pets: [] });

	let predictedBreed = predictions[0].className.split(",")[0]

	let a = predictedBreed.toLowerCase()
	let b = a.charAt(0).toUpperCase() + a.slice(1);

	const pets = await Pet.findObjects({ breed: b })
	const result = await Pet.exportObjects(pets[1])

	return res.sendResponse({ length: result.length, pets: result });
});



module.exports = router;
