const mongoose = require('mongoose');
const admin = require('firebase-admin');


const sendPush = (tokens, title, body, payload = {}) => {
    const data = payload;
    const notification = { sound: 'default' };

    if (body) {
        notification.body = body;
        data.body = body
    }

    if (title) {
        notification.title = title;
        data.title = title
    }

    return admin.messaging().sendToDevice(tokens, { data, notification }, {});
}

const sendNotificationToUser = async (notification, payload = {}) => {
    const user = await mongoose.models.User.findById( notification.user_id );

    if (!user || !user.fcm_tokens || user.fcm_tokens.lenght === 0) return;
    console.log("OK")
    return sendPush(user.fcm_tokens, notification.title, notification.body, payload)
}

const sendAssessmentRequestPush = async (notification) => {
    const payload = {
        category: notification.category,
        assessment_campaign_user_id: notification.payload.assessment_campaign_user_id
    }

    return sendNotificationToUser(notification, payload)
}


module.exports = { 
    sendPush,
    sendNotificationToUser,
    sendAssessmentRequestPush
};
