const mongoose = require('mongoose');
const paramsApi = require('./params');
const jwt = require('jsonwebtoken');
const utils = require("./utils");
const config = require("../config/config");

const { USER_STATUSES } = utils.getConstants();

const getAuthenticationToken = (req) => {
	return (req.headers.authorization || "").replace("Bearer ", "");
}

const user = async (req, res, next) => {
  const accessToken = getAuthenticationToken(req);
  
 if (!accessToken) return unauthorized(res);
  
  let decoded;

  try{
    decoded = jwt.verify(accessToken, config.authorization.token_secret);
  } catch (error){
    return unauthorized(res);
  }

  if (new Date(decoded.exp * 1000) < new Date()) return unauthorized(res);

  const user = await mongoose.models.User.findOne({access_tokens: accessToken});

  if (!user) return unauthorized(res);
  if (!user._id.equals(decoded.data)) return unauthorized(res);

  if (!user.status === USER_STATUSES.blocked) return forbidden(res);

  req.user = user;

  return next();
};

const unauthorized = (res) => {
	return res.sendStatus(401)
}

const forbidden = (res) => {
  return res.sendStatus(403)
}


module.exports = { user, admin, forbidden };
