const constants = require("../../shared/constants.js");
const Pusher = require('pusher');

const pusher = new Pusher({      
  appId: '1072099',      
  key: 'dd15d169c62d9c16f8a3',      
  secret: '13d9d9735816df93b288',      
  cluster: 'eu'    
});

const sendError = (res, message) => {
  return res.json({ success: false, message }).status(500);
};

const sendResponse = (res, data) => {
  const result = { success: true };
  if (data) result.data = data;
  return res.json(result);
};

const getConstants = () => {
	return constants;
};

module.exports = {
  sendError,
  sendResponse,
  getConstants,
  pusher,
};
