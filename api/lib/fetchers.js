const mongoose = require('mongoose');

const pet = async (req, res, next) => {
  req.pet = await mongoose.models.Pet.findById(req.params.pet_id);
  return next();
};

module.exports = { 
	pet,
};
