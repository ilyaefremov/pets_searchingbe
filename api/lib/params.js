const mongoose = require('mongoose');
const masterConfig = require('../config/config');

const environment = process.env.ENV || 'dev';
const serverConfig = masterConfig.server[environment];

const get = () => {
  const environment = process.env.ENV || 'dev';
  return mongoose.models.Params.findOne({ environment });
};

const getServerConfig = (name) => {
	return serverConfig[name];
}

const getByName = async name => {
  const params = await get();
  return params[name] || {};
};

module.exports = { get, getByName, getServerConfig };