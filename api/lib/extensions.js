const paramsApi = require('./params');
const firebase = require('./firebase-api');
const accountManager = require('./account-manager');

module.exports = { paramsApi, accountManager, firebase };
