const jwt = require('jsonwebtoken');
const paramsApi = require("./params.js");
const mongoose = require('mongoose');

const refreshToken = async (user) => {
  const config = await paramsApi.getByName("login_token");
  const { secret, expires } = config.customer;
 
  user.access_token = jwt.sign({data: user._id}, secret, { expiresIn: expires })

  await user.save();
  return user.access_token;
}

const logout = async (user) => {
	user.access_token = null;
 	return user.save();
}


module.exports = {
  logout,
  refreshToken
};