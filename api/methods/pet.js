const mongoose = require('mongoose');
const admin = require('firebase-admin');
const jwt = require("jsonwebtoken");
const utils = require("../lib/utils");
const { PET_STATUSES, GENDER, DEFAULT_PAGE_SIZE } = utils.getConstants();

const config = require("../config/config");

module.exports = function(schema) {

    const EDITABLE_FIELDS = ["name", "breed_id", "color", "age", "gender", "description", "latitude", "longitude", "image_id"];
    const FILTER_FIELDS = ["name", "breed_id", "color", "age", "gender", "status"];

    schema.methods.exportObject = async function (){
        const pet = this;
        const user = await mongoose.models.User.findById(pet.user_id)
        const breed = await mongoose.models.Breed.findById(pet.breed_id)
        
        let images = []

        for (let i = 0; i < pet.secondary_images.length; i++){
            images[i] = { image_url: await mongoose.models.FilePointer.getImageUrl(pet.secondary_images[i].image_id) }
        }

        return {
            id: pet._id,
            name: pet.name,
            breed: breed.breed,
            color: pet.color,
            age: pet.age,
            description: pet.description,
            longitude: pet.longitude,
            latitude: pet.latitude,
            image_url: await mongoose.models.FilePointer.getImageUrl(pet.image_id),
            secondary_images: images,
            gender: pet.gender,
            status: pet.status,
            is_archived: pet.is_archived,
            user: await user.exportObject(),
            created_at: pet.created_at,
        }
    }

    schema.statics.exportObjects = async function(objects = []){
        return Promise.all(objects.map(object => object.exportObject()));
    }

    schema.statics.createObject = async function(data = [], user){
        const breed = await mongoose.models.Breed.findOne({ localized_breeds: { $elemMatch: { localized_breed: data.breed } } })

        if (!breed) return false

        data.breed_id = breed._id
        delete data.breed

        return this.create({...data, user_id: user._id});
    }

    schema.statics.findObjects = async function(data = {}, page = 1, limit = DEFAULT_PAGE_SIZE){
        const [intPage, intLimit] = [parseInt(page), parseInt(limit)];

        if (data.breed){
            const breed = await mongoose.models.Breed.findOne({ localized_breeds: { $elemMatch: { localized_breed: data.breed } } })

	       if (breed) data.breed_id = breed._id
	       delete data.breed
        }

        for (const field of Object.keys(data)){
            if (data[field].length == 0 || !FILTER_FIELDS.includes(field)) delete data[field]
        }

        return [await this.count({ is_archived: false }), await this.find({ ...data, is_archived: false }).limit(intLimit).skip(intLimit * (intPage - 1) ).sort('-created_at')];
    }

    schema.statics.findMapObjects = async function(data = {}){
        const { ne_lat: neLat, ne_lng: neLng, sw_lat: swLat, sw_lng: swLng } = data

        return this.find({ latitude: { $gt :  swLat, $lt : neLat }, longitude: { $gt :  swLng, $lt : neLng } })
    }

    schema.statics.getArchived = async function(user_id){
        return this.find({ user_id: user_id, is_archived: true }).sort('-created_at');
    }

    schema.methods.changeStatus = async function(){
        const pet = this;

        pet.status = (pet.status === PET_STATUSES.found) ? PET_STATUSES.lost : PET_STATUSES.found
        pet.is_archived = pet.is_archived ? false : true

        return pet.save()
    }

    schema.methods.updateObject = async function (data = {}){
        const pet = this

        if (data.breed){
            const breed = await mongoose.models.Breed.findOne({ localized_breeds: { $elemMatch: { localized_breed: data.breed } } })

            if (breed) data.breed_id = breed._id
            delete data.breed
        }

        for (const field of Object.keys(data)){
            if (EDITABLE_FIELDS.includes(field)) pet[field] = data[field];
        }

        return pet.save();
    }

    schema.methods.updateImages = async function (imagesId = []){
        const pet = this

        for (imageId of imagesId){
            pet.secondary_images.push({ image_id: imageId.image_id })
        }

        return pet.save();
    }

    schema.methods.deleteImage = async function(imageUrl){
        const pet = this
        const filePointer = await mongoose.models.FilePointer.getId(imageUrl)
        
        for (let i = 0; i < pet.secondary_images.length; i++){
            if (pet.secondary_images[i].image_id.equals(filePointer.id)){
                pet.secondary_images.splice(i, 1)
            }
        }

        return await pet.save()
    }

    schema.methods.deleteObject = async function(){
        const pet = this
    	const users = await mongoose.models.User.find({ saved_pets: { $elemMatch: { pet_id: pet.id } } })

    	for (user of users){
    	    await user.saved_pets.filter( async function(value, index, arr){

        		if (value.pet_id.equals(pet.id)){
        		    user.saved_pets.splice(index, 1)
        		    await user.save()
    	        }
    	    })
    	}

        return await pet.remove()
    }

}
