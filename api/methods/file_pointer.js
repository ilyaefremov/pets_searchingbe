const config = require('../config/config');

const USER_IMAGES_FOLDER = "users_images";

module.exports = function(schema) {

    schema.methods.exportObject = function(){
        const filePointer = this;
        return { id: filePointer._id, url: filePointer.getUrl() }
    }

    schema.statics.getImageUrl = async function(filePointerId){
        if (!filePointerId) return null;
        
        const filePointer = await this.findById(filePointerId);
        if (!filePointer) return null;

        return filePointer.getUrl()
    }

    schema.methods.getUrl = function(){
        const filePointer = this;
        return `${config.server.dev.root_url}${filePointer.path}`;
    }

    schema.statics.getId = async function(url){
        const path = url.replace(config.server.dev.root_url, "");
        return this.findOne({path: path})
    }

    schema.statics.createObject = async function(path){
        return this.create({ path });
    }

}
