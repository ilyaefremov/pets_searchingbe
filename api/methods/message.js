const mongoose = require('mongoose');
const admin = require('firebase-admin');
const jwt = require("jsonwebtoken");
const utils = require("../lib/utils");
const {firebase} = require('../lib/extensions');

const config = require("../config/config");

module.exports = function(schema) {

	schema.statics.getMessages = async function(chat_id){
		if (chat_id) return this.find({ chat_id: chat_id })
    }

	schema.statics.createObject = async function(message, user, chatId){
		return this.create({ message, user_id: user._id, chat_id: chatId})
    }

    schema.statics.sendPush = async function(message, chatId){

	    const chat = await mongoose.models.Chat.findById(chatId)
        const recipient = await chat.getRecipient(new String(message.user.id)) 

        const notification = { 
            user_id: recipient._id,
            title: "New message",
            body: message.user.name + ": " + message.text,
            payload: { },
        };

        return firebase.sendNotificationToUser(notification)
    }

    schema.methods.exportObject = async function (){
        const message = this;
        const user = await mongoose.models.User.findById(message.user_id)

        return {
            id: message._id,
            text: message.message,
            chat_id: message.chat_id,
            is_red: message.is_red,
            user: await user.exportObject(),
            created_at: message.created_at,
        }
    }

    schema.statics.exportObjects = async function(objects = []){
        return Promise.all(objects.map(object => object.exportObject()));
    }

    schema.statics.setRed = async function(chat){
        return this.updateMany({ chat_id: chat.id, user_id: chat.user.id  }, { $set: { is_red: true } })
    }
}
