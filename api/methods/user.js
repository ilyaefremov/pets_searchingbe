const mongoose = require('mongoose');
const admin = require('firebase-admin');
const jwt = require("jsonwebtoken");
const utils = require("../lib/utils");

const { USER_STATUSES, GENDER } = utils.getConstants();

const config = require("../config/config");

module.exports = function(schema) {

    const EDITABLE_FIELDS = ["name", "gender", "age", "image_id"];

    schema.methods.refreshToken = async function () {
        const user = this;

        const accessToken = jwt.sign({data: user._id}, config.authorization.token_secret, { expiresIn: config.authorization.token_expires_in });
        
        user.access_tokens.push(accessToken);
        await user.save();

        return accessToken;
    }

    schema.methods.refreshFcmToken = async function (fcmToken){
        const user = this;

        user.fcm_tokens.push(fcmToken);
        
        return user.save();
    }

    schema.methods.updateObject = async function (data = {}){
        const user = this

        for (const field of Object.keys(data)){
            if (EDITABLE_FIELDS.includes(field)) user[field] = data[field];
        }

        return user.save();
    }

    schema.methods.exportObject = async function (){
        const user = this;

        return {
            id: user._id,
            name: user.name,
            country: user.country,
            state: user.state,
            city: user.city,
            gender: user.gender,
            age: user.age,
            image_url: await mongoose.models.FilePointer.getImageUrl(user.image_id),
            is_onboarding_completed: user.is_onboarding_completed
        }
    }

    schema.methods.logoutUser = async function (accessToken, fcmToken){
        const user = this;

        const indexAccessToken = user.access_tokens.indexOf(accessToken);
        const indexFcmToken = user.fcm_tokens.indexOf(fcmToken);

        if (indexAccessToken > -1) user.access_tokens.splice(indexAccessToken, 1);
        if (indexFcmToken > -1) user.fcm_tokens.splice(indexFcmToken, 1);

        return user.save();
    }

    schema.methods.exportBookmarks = async function (){
        const user = this;
        let bookmarks = [];

        for (let i = 0; i < user.saved_pets.length; i++){
            let pet = await mongoose.models.Pet.findById(user.saved_pets[i].pet_id);
            bookmarks.push(await pet.exportObject());
        }

        bookmarks.sort(function(a, b) { 
            if (a.created_at > b.created_at) return 1;
            if (a.created_at < b.created_at) return -1;
            return 0; 
        });

        return bookmarks
    }

    schema.statics.createObject = async function (body = {}){
        if (!body.firebase_id_token) throw new Error("firebase_id_token is empty")

        const decodedToken = await admin.auth().verifyIdToken(body.firebase_id_token);
        const query = { firebase_user_id: decodedToken.uid };

        const user = await this.findOne(query);
        if (user) return user;

        return this.create(query);
    }

}
