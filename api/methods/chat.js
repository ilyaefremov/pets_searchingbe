const mongoose = require('mongoose');
const admin = require('firebase-admin');
const jwt = require("jsonwebtoken");
const utils = require("../lib/utils");

const config = require("../config/config");

module.exports = function(schema) {

    schema.methods.exportObject = async function (current_user, usersOnline = []){
        const chat = this;

        const user = await chat.getRecipient(new String(current_user._id))
        const recipient = await user.exportObject()
        recipient.is_online = usersOnline.includes(String(recipient.id))

        const messages = await mongoose.models.Message.find({ chat_id: chat._id }).sort('-created_at');
        const unreadMessages = messages.filter(message => message.is_red === false)

        return {
            id: chat._id,
            user: recipient,
            created_at: chat.created_at,
            unread_messages: unreadMessages.length,
            last_message: messages.length != 0 ? await messages[0].exportObject() : null,
        }
    }

    schema.methods.getRecipient = async function(current_user_id){
	    const chat = this;
        for (let i = 0; i < chat.user_ids.length; i ++) {
            let user_id = new String(chat.user_ids[i].user_id)

            if (current_user_id.localeCompare(user_id) != 0){
                return await mongoose.models.User.findById(user_id)
            }
        
        }
    }

    schema.statics.getUsersOnline = async function(){
        let promise = new Promise((resolve, reject) => {
            utils.pusher.get({ path: '/channels/presence-common-channel/users', params: {} },
                function(error, request, response) {
                    if (response.statusCode === 200) {
                        const res = JSON.parse(response.body);
                        const ids = res.users.map(function(x){return x.id})
                        resolve(ids)
                    }
                }
            )
        });
        return promise
    }

    schema.statics.findObjects = async function(user){
        return mongoose.models.Chat.find({user_ids: {$elemMatch: {user_id: user._id}}})
    }

    schema.statics.isChatExist = async function(user){
        return mongoose.models.Chat.find({ $and: [ { user_ids: { $elemMatch : { user_id: user._id }}}, { user_ids: { $elemMatch : { user_id: recipientId }}}] });
    }

    schema.statics.exportObjects = async function(objects = [], user){
        const usersOnline = await this.getUsersOnline()

        return Promise.all(objects.map(object => object.exportObject(user, usersOnline)));
    }

    schema.statics.createObject = async function(user_ids = []){
        return this.create({ user_ids: user_ids });
    }

}

