const USER_STATUSES = {
    active: "active",
    blocked: "blocked"
}

const GENDER = {
	male: "male",
	female: "female"
}

const PET_STATUSES = {
	lost: "lost",
	found: "found"
}

const DEFAULT_PAGE_SIZE = 7;

module.exports = { 
	USER_STATUSES,
	GENDER,
	PET_STATUSES,
	DEFAULT_PAGE_SIZE
};