module.exports = function(mongoose, methodsPath){
	require('./models/user')(mongoose, methodsPath);
	require('./models/pet')(mongoose, methodsPath);
	require('./models/file_pointer')(mongoose, methodsPath);
	require('./models/chat')(mongoose, methodsPath);
	require('./models/message')(mongoose, methodsPath);
	require('./models/breed')(mongoose, methodsPath);
}