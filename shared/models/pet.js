const { PET_STATUSES, GENDER } = require("../constants.js");

const scriptName = require('path').basename(__filename);

module.exports = function(mongoose, methodsPath) {
	const { Schema } = mongoose;

	const schema = new Schema({
		name: { type: String, required: true },
		breed_id: { type: Schema.ObjectId, ref: 'Breed', required: true },
		color: { type: String, required: true },
		age: { type: String, required: true },
		description: { type: String, required: true },
		longitude: { type: String, required: true },
		latitude: { type: String, required: true },
		image_id: { type: Schema.ObjectId, ref: 'FilePointer' },
		secondary_images: [{
	    	image_id: { type: Schema.ObjectId, ref: 'FilePointer'  }
	    }],
		gender: { type: String, enum: Object.values(GENDER) },
		is_archived: { type: Boolean, required: true, default: false },
		status: {type: String, require: true, enum: Object.values(PET_STATUSES), default: PET_STATUSES.lost },
		user_id: { type: Schema.ObjectId, ref: 'User', required: true },
	    created_at: { type: Date, default: Date.now},
	});

	require(`${methodsPath}/${scriptName}`)(schema);

	return mongoose.model('Pet', schema);
}
