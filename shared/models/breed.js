const scriptName = require('path').basename(__filename);

module.exports = function(mongoose, methodsPath) {
	const { Schema } = mongoose;

	const schema = new Schema({
	    breed: { type: String, required: true, unique: true },
	    localized_breeds: [{
	    	localized_breed: { type: String }
	    }],
	    created_at: { type: Date, default: Date.now},
	});

	require(`${methodsPath}/${scriptName}`)(schema);

	return mongoose.model('Breed', schema);
}
