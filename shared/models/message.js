const scriptName = require('path').basename(__filename);

module.exports = function(mongoose, methodsPath) {
	const { Schema } = mongoose;

	const schema = new Schema({
		user_id: { type: Schema.ObjectId, ref: 'User' },
		chat_id: { type: Schema.ObjectId, ref: 'Chat' },
		message: { type: String, required: true },
		is_red: { type: Boolean, required: true, default: false },
	    created_at: { type: Date, default: Date.now},
	});

	require(`${methodsPath}/${scriptName}`)(schema);

	return mongoose.model('Message', schema);
}
