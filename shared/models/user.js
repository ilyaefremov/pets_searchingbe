const { USER_STATUSES, GENDER } = require("../constants.js");

const scriptName = require('path').basename(__filename);

module.exports = function(mongoose, methodsPath) {
	const { Schema } = mongoose;

	const schema = new Schema({
	    access_tokens: { type: [String], default: [] },
	    name: { type: String },
	    age: { type: Number },
	    status: {type: String, require: true, enum: Object.values(USER_STATUSES), default: USER_STATUSES.active },
	    gender: { type: String, enum: Object.values(GENDER) },
	    image_id: { type: Schema.ObjectId, ref: 'FilePointer' },
	    firebase_user_id: { type: String, required: true, unique: true },
	    fcm_tokens: { type: [String], default: [] },
	    country: { type: String, required: false },
		state: { type: String, required: false },
		city: { type: String, required: false },
		saved_pets: [{
	    	pet_id: { type: Schema.ObjectId, ref: 'Pet' }
	    }],
	    created_at: { type: Date, default: Date.now},
	});

	require(`${methodsPath}/${scriptName}`)(schema);

	return mongoose.model('User', schema);
}
