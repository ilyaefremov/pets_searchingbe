const scriptName = require('path').basename(__filename);

module.exports = function(mongoose, methodsPath) {
	const { Schema } = mongoose;

	const schema = new Schema({
		user_ids: [{
	    	user_id: { type: Schema.ObjectId, ref: 'User', required: true }
	    }],
	    created_at: { type: Date, default: Date.now},
	});

	require(`${methodsPath}/${scriptName}`)(schema);

	return mongoose.model('Chat', schema);
}
